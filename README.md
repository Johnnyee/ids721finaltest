# IDS721 Final Project Group 12

## Project Overview
In this project, we will focus on the practical application of machine learning by operationalizing an open-source model. The core task is to serve the model through a robust web service developed using the Rust programming language. The project encompasses several key aspects of modern software engineering, including containerization, automated deployment via CI/CD pipelines, and effective monitoring and documentation, like we complete with configuration files for deployment using AWS's managed Kubernetes service (EKS). A successful demonstration of the application via a YouTube video is also required to showcase the functionality and utility of the service.

## Getting Started 

### Prerequsites 

Before you begin, ensure you have the following installed and configured:

- **Rust**: A system programming language known for its memory safety and high performance. It is ideal for system-level tooling, application development, and especially useful in scenarios demanding high concurrency and safety. Ensure Rust is installed for building the web service.

- **Rust-Bert**: A Rust library for comprehensive NLP pipelines. This library will be used for handling the machine learning model.

- **Actix**: A Rust framework for building high-performance web services, used for developing the web application.

- **Docker**: A platform-as-a-service product that facilitates the packaging and distribution of applications in lightweight containers. Docker containers encapsulate the application and its environment, ensuring consistency across multiple development and release cycles.

- **Kubernetes**: An open-source system for automating deployment, scaling, and management of containerized applications. It helps with managing containerized applications in different deployment environments and is crucial for orchestrating Docker containers.

- **Hugging Face**: A repository of machine learning models. You will need access to select and download pre-trained models.

- **AWS Elastic Container Registry (ECR)**: A Docker container registry that simplifies the management and deployment of Docker images. Necessary for storing and managing the Docker images created.

- **AWS EKS**: A managed Kubernetes service for easy deployment and management of serverless functions in the AWS cloud. This will be used for deploying the containerized service.

- **Git**: A distributed version control system to track changes in source code during software development. It's essential for managing project files and collaborating on code development, especially when integrated with CI/CD pipelines for automated processes.

## Building

### Step 1:

To run this application locally, ensure you have `Rust` and `Cargo` installed.

1. **Clone the Repository**
   ```
   git clone https://github.com/yourusername/rust-model-serving.git
   cd rust-model-serving
   ```

2. **Navigate to the Source Code**
   ```
   cd final_llmops
   ```

3. **Direct Execution**
   This command starts the application locally on port 8080:
   ```
   cargo run
   ```


### Step 2:

Containerized locally

1. **Build the Docker Image**
   Navigate to the project root and execute:
   ```
   docker build -t final_llmpos .
   ```

2. **Run the Container**
   This command runs the container and exposes it on local port 8080:
   ```
   docker run -p 8080:8080 -v <model path in your computer> idsfinal
   ```

### Step 3:

 AWS Deployment

Deployment on AWS involves using AWS EKR for image storage and AWS EKS for serverless function deployment.

1. **AWS ECR Setup**
   Create a public AWS ECR repository:
   

2. **Docker Image Handling**
   Authenticate Docker with AWS and manage the Docker image:
   ```
   aws ecr get-login-password --region <primary region> | docker login --username AWS --password-stdin <repo-id>.dkr.ecr.us-east-1.amazonaws.com
   docker build -t transformer .
   docker tag transformer:latest <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
   docker push <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
   ```

3. **Cluster Deployment on AWS EKS**
   Use eksctl to create and manage the Kubernetes cluster:
   ```
   eksctl create cluster --name <cluster name> --region <cluster region>
   cd EKS
   kubectl apply -f eks-sample-deployment.yaml
   kubectl apply -f eks-sample-service.yaml  
   ```

## Application Usage

### Sentiment Analysis Endpoint
Post a JSON body to the sentiment analysis endpoint to analyze text sentiment:
```
{
    "text": "The tower is 324 metres (1,063 ft) tall, about the same height as an 81-storey building, and the tallest structure in Paris. Its base is square, measuring 125 metres (410 ft) on each side. During its construction, the Eiffel Tower surpassed the Washington Monument to become the tallest man-made structure in the world, a title it held for 41 years until the Chrysler Building in New York City was finished in •1930. It was the first structure to reach a height of•300 metres. Due to the addition of a broadcasting aerial at the top of the tower in 1957, it is now taller than the Chrysler Building by 5.2 metres (17 ft). Excluding transmitters, the Eiffel Tower is the second tallest free-standing structure in France after the Millau Viaduct."
}
```
Response example:
```
{
    "summary": "The Eiffel Tower in Paris has been officially opened to the public for the first time them"
}
```

### HTTP Endpoint Configuration
Configure an HTTP endpoint via AWS Route53 and manage HTTPS through AWS ACM to ensure secure connections.

### Logging and Monitoring
Enable logging with eksctl and monitor through AWS CloudWatch.

![Cloud_formation](https://cdn.mathpix.com/snip/images/1NwNv8lBPT2no0fJp0poi9XD6OPOCXt-lnFpgp64Ccc.original.fullsize.png)

![Cloudwatch](https://cdn.mathpix.com/snip/images/TpLi0swojMN-yPToD4vOALApZ9LXjo3UtO7mYwz7y54.original.fullsize.png)


![EC2](https://cdn.mathpix.com/snip/images/l5zNVAb7fa4nuIda9ndAjKMQ6jGZlOm01jDroKYX6Xo.original.fullsize.png)


![ECR](https://cdn.mathpix.com/snip/images/B4DBbj-J6qFzZVyxxbhy2vWWRYkUhou4wZk_n_r_vEk.original.fullsize.png)

![EKS_Pods](https://cdn.mathpix.com/snip/images/iQpyG1oCqY_Tbht1uL8RMeLbxBzuqd6aKA6VwCu0x48.original.fullsize.png)


![EKS](https://cdn.mathpix.com/snip/images/27GZ0jTvmDHlhfy3PkhWLiKkobr9y-j0Nv5eCbiA0TY.original.fullsize.png)

![load_balancer_new](https://cdn.mathpix.com/snip/images/jsAM7DACmHADo1wvGXe1YmoV9jLYw4YlHJHIvkZZu0g.original.fullsize.png)


![model_inference_local_test](https://cdn.mathpix.com/snip/images/IS1L31yKD5S75Zz9yJ-tQVRXKyA7lraUb1kHcPLd8n4.original.fullsize.png)

![postman_for_demo_new](https://cdn.mathpix.com/snip/images/gYxVjgekFZtn4BJi0DRcvL66KwRfqwkD-OtSvIJAsxE.original.fullsize.png)


![postman_new](https://cdn.mathpix.com/snip/images/vELsXVSeop3TlLaNREj-5buH6rBYby4LADmOeUwwd_Q.original.fullsize.png)

